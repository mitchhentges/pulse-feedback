extern crate libpulse_binding;

use libpulse_binding::mainloop::standard::{Mainloop, IterateResult};
use libpulse_binding::context::Context;
use libpulse_binding::context::flags::NOAUTOSPAWN;
use libpulse_binding::error::PAErr;
use std::error::Error;
use std::fmt;
use std::fmt::Formatter;
use libpulse_binding::context;
use std::cell::RefCell;
use std::rc::Rc;
use libpulse_binding::stream::{Stream, SeekMode, PeekResult};
use libpulse_binding::stream::flags::{ADJUST_LATENCY, NOFLAGS};
use libpulse_binding::sample::{Spec, Format};
use libpulse_binding::def::BufferAttr;
use libpulse_binding::time::MicroSeconds;
use std::f32::consts::PI;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
use std::time::SystemTime;

const DEFAULT_SAMPLE_SPEC: Spec = Spec {
    channels: 1,
    rate: 44100,
    format: Format::S16le,
};

struct Wave {
    clock: f32,
}

impl Wave {
    fn new() -> Wave {
        Wave {
            clock: 0 as f32,
        }
    }

    fn next_value(&mut self) -> f32 {
        let sample_rate = DEFAULT_SAMPLE_SPEC.rate as f32;
        self.clock = (self.clock + 1.0) % sample_rate;
        (self.clock * 440.0 * 2.0 * PI / sample_rate).sin()
    }
}

fn init_playback(context: Rc<RefCell<Context>>, rx: Receiver<i16>) {
    let context = context.clone();
    let stream = Stream::new(&mut *context.borrow_mut(), "playback", &DEFAULT_SAMPLE_SPEC, None).unwrap();
    let stream_rc = Rc::new(RefCell::new(stream));
    let stream = stream_rc.clone();
    let mut stream = stream.borrow_mut();
    let playback_buffer_attr = BufferAttr {
        fragsize: std::u32::MAX, // only matters for recording
        maxlength: std::u32::MAX,
        prebuf: std::u32::MAX,
        minreq: std::u32::MAX,
        tlength: DEFAULT_SAMPLE_SPEC.usec_to_bytes(MicroSeconds(10000)) as u32,
    };

    let mut wave = Wave::new();

    stream.set_overflow_callback(Some(Box::new(move || {
        println!("[playback] overflow");
    })));
    stream.set_underflow_callback(Some(Box::new(move || {
        println!("[playback] underflow");
    })));
    stream.set_started_callback(Some(Box::new(move || {
        println!("[playback] started");
    })));
    stream.set_suspended_callback(Some(Box::new(move || {
        println!("[playback] suspended");
    })));
    stream.set_event_callback(Some(Box::new(move |_, _| {
        println!("[playback] event???");
    })));
    stream.set_moved_callback(Some(Box::new(move || {
        println!("[playback] it moved how neat is that");
    })));
    stream.connect_playback(None, Some(&playback_buffer_attr), ADJUST_LATENCY, None, None).unwrap();
    stream.set_write_callback(Some(Box::new(move |bytes| {
        let mut stream = stream_rc.borrow_mut();
        let buffer = stream.begin_write(Some(bytes)).unwrap().unwrap();

        for i in 0..buffer.len() / 2 {
            let sample = match rx.try_recv() {
                Ok(sample) => sample,
                Err(_) => (wave.next_value() * std::i16::MAX as f32) as i16,
            };
            // let sample = (wave.next_value() * std::i16::MAX as f32) as i16;
            let u8_samples = sample.to_le_bytes();
            buffer[i * 2] = u8_samples[0];
            buffer[i * 2 + 1] = u8_samples[1];
        }

        stream.write_copy(&buffer, 0, SeekMode::Relative).unwrap();
    })));
}

fn init_record(context: Rc<RefCell<Context>>, tx: Sender<i16>) {
    let context = context.clone();
    let stream = Stream::new(&mut *context.borrow_mut(), "record", &DEFAULT_SAMPLE_SPEC, None).unwrap();
    let stream_rc = Rc::new(RefCell::new(stream));
    let stream = stream_rc.clone();
    let mut stream = stream.borrow_mut();
    let record_buffer_attr = BufferAttr {
        tlength: std::u32::MAX, // only matters for playback
        maxlength: std::u32::MAX,
        prebuf: std::u32::MAX,
        minreq: std::u32::MAX,
        fragsize: 0 as u32,
    };

    stream.set_overflow_callback(Some(Box::new(move || {
        println!("[recording] overflow");
    })));
    stream.set_underflow_callback(Some(Box::new(move || {
        println!("[recording] underflow");
    })));
    stream.set_started_callback(Some(Box::new(move || {
        println!("[recording] started");
    })));
    stream.set_suspended_callback(Some(Box::new(move || {
        println!("[recording] suspended");
    })));
    stream.set_event_callback(Some(Box::new(move |_, _| {
        println!("[recording] event???");
    })));
    stream.set_moved_callback(Some(Box::new(move || {
        println!("[recording] it moved how neat is that");
    })));
    stream.connect_record(None, Some(&record_buffer_attr), ADJUST_LATENCY).unwrap();
    stream.set_read_callback(Some(Box::new(move |_| {
        let mut stream = stream_rc.borrow_mut();
        match stream.peek() {
            Ok(data) => {
                match data {
                    PeekResult::Empty => println!("empty"),
                    PeekResult::Hole(_) => println!("hole detected"),
                    PeekResult::Data(data) => {
                        // println!("[recording] {}", data.len());
                        for i in 0..(data.len() / 2) {
                            let sample = i16::from_le_bytes([
                                data[i * 2],
                                data[i * 2 + 1]
                            ]);
                            tx.send(sample).unwrap();
                        }
                    }
                }
            },
            Err(_) => {
                panic!("Errored in set_read_callback");
            }
        }
        stream.discard().unwrap();
    })));
}

fn main() {
    let mut mainloop = Mainloop::new().unwrap();
    let mut context = Context::new(&mainloop, "pulse-feedback").unwrap();
    context.connect(Some("/run/user/1000/pulse/native"), NOAUTOSPAWN, None).unwrap();
    context.wait(&mut mainloop).unwrap();
    let context = Rc::new(RefCell::new(context));
    let (tx, rx) = mpsc::channel();
    init_record(context.clone(), tx);
    init_playback(context.clone(), rx);
    mainloop.run().unwrap();
}

#[derive(Debug)]
enum WaitError {
    Failed(Option<PAErr>),
    Cancelled,
}

impl Error for WaitError {}

impl fmt::Display for WaitError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), ::std::fmt::Error> {
        match *self {
            WaitError::Failed(_) => f.write_str("Failed"),
            WaitError::Cancelled => f.write_str("Cancelled"),
        }
    }
}

trait WaitableOperation {
    fn wait(&self, mainloop: &mut Mainloop) -> Result<(), WaitError>;
}

impl WaitableOperation for Context {
    fn wait(&self, mainloop: &mut Mainloop) -> Result<(), WaitError> {
        loop {
            match mainloop.iterate(false) {
                IterateResult::Quit(_) => {
                    return Err(WaitError::Cancelled);
                }
                IterateResult::Err(err) => {
                    return Err(WaitError::Failed(Some(err)));
                }
                IterateResult::Success(_) => {}
            };
            match self.get_state() {
                context::State::Ready => { break; }
                context::State::Failed |
                context::State::Terminated => {
                    return Err(WaitError::Failed(None));
                }
                _ => {}
            }
        }
        Ok(())
    }
}